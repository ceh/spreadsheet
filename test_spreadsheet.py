import unittest
import spreadsheet

__author__ = "charles"


class TestParseFile(unittest.TestCase):
    def test_cell_name(self):
        self.assertEqual(spreadsheet.cell_name(1, 1), "a1")
        self.assertEqual(spreadsheet.cell_name(2, 1), "a2")
        self.assertEqual(spreadsheet.cell_name(1, 2), "b1")
        self.assertEqual(spreadsheet.cell_name(26, 26), "z26")
        self.assertEqual(spreadsheet.cell_name(27, 27), "aa27")
        self.assertEqual(spreadsheet.cell_name(28, 28), "ab28")
        self.assertEqual(spreadsheet.cell_name(702, 702), "zz702")
        self.assertEqual(spreadsheet.cell_name(703, 703), "aaa703")

    def test_parse_cell(self):
        self.assertEqual(spreadsheet.parse_cell("b1 b2 +"), ["b1", "b2", "+"])
        self.assertEqual(spreadsheet.parse_cell("2 b2 3 * -"), ["2", "b2", "3", "*", "-"])
        self.assertEqual(spreadsheet.parse_cell("3"), ["3"])
        self.assertEqual(spreadsheet.parse_cell("+"), ["+"])
        self.assertEqual(spreadsheet.parse_cell("a1"), ["a1"])
        self.assertEqual(spreadsheet.parse_cell("5"), ["5"])
        self.assertEqual(spreadsheet.parse_cell(""), [])
        self.assertEqual(spreadsheet.parse_cell("7 2 /"), ["7", "2", "/"])
        self.assertEqual(spreadsheet.parse_cell("c2 3 *"), ["c2", "3", "*"])
        self.assertEqual(spreadsheet.parse_cell("1 2"), ["1", "2"])
        self.assertEqual(spreadsheet.parse_cell("5 1 2 + 4 * + 3 -"), ["5", "1", "2", "+", "4", "*", "+", "3", "-"])

    def test_parse_line(self):
        self.assertEqual(spreadsheet.parse_line("b1 b2 +,2 b2 3 * -,3 ,+"), ["b1 b2 +", "2 b2 3 * -", "3", "+"])
        self.assertEqual(spreadsheet.parse_line("a1     ,5         ,  ,7 2 /"), ["a1", "5", "", "7 2 /"])
        self.assertEqual(spreadsheet.parse_line(
            "c2 3 * ,1 2       ,  ,5 1 2 + 4 * + 3 -"),
            ["c2 3 *", "1 2", "", "5 1 2 + 4 * + 3 -"])

    def test_Spreadsheet(self):
        s = spreadsheet.Spreadsheet(
            ["b1 B2 +,2 b2 3 * -,3 ,+\n",
             "a1     ,5         ,  ,7 2 /\n",
             "C2 3 * ,1 2       ,  ,5 1 2 + 4 * + 3 -\n"
             ])
        self.assertEqual(s.d,
                         {
                             "a1": ["b1", "b2", "+"],
                             "b1": ["2", "b2", "3", "*", "-"],
                             "c1": ["3"],
                             "d1": ["+"],
                             "a2": ["a1"],
                             "b2": ["5"],
                             "d2": ["7", "2", "/"],
                             "a3": ["c2", "3", "*"],
                             "b3": ["1", "2"],
                             "d3": ["5", "1", "2", "+", "4", "*", "+", "3", "-"],
                         }
                         )
        self.assertEqual(s.lines,
                         [
                             [["b1", "b2", "+"], ["2", "b2", "3", "*", "-"], ["3"], ["+"]],
                             [["a1"], ["5"], [], ["7", "2", "/"]],
                             [["c2", "3", "*"], ["1", "2"], [], ["5", "1", "2", "+", "4", "*", "+", "3", "-"]]
                         ])

    def test_non_rectangular(self):
        s = spreadsheet.Spreadsheet(
            ["1,2,3\n",
             "1,2,3,4,5\n",
             "1\n"]
        )
        self.assertEquals(str(s),
                          "1,2,3\n" +
                          "1,2,3,4,5\n" +
                          "1\n")

    def test_empty_row(self):
        s = spreadsheet.Spreadsheet(
            ["\n",
             "1,2,3,4,5\n",
             "1 1 +\n"]
        )
        self.assertEquals(str(s),
                          "0\n" +
                          "1,2,3,4,5\n" +
                          "2\n")

    def test_empty_file(self):
        self.assertEquals(str(spreadsheet.Spreadsheet([])), "")

    def test_evalCell(self):
        s = spreadsheet.Spreadsheet([
            "11, 21, 31\n",
            "12, 22, 32\n",
            "13, 23, 33\n",
            "a1, a4, b4\n",
            "a5, c5, b5\n"])
        self.assertEqual(s.eval_cell([]), 0)
        self.assertEqual(s.eval_cell(["d4"]), 0)
        self.assertEqual(s.eval_cell(["99"]), 99)
        self.assertEqual(s.eval_cell(["a1"]), 11)
        self.assertEqual(s.eval_cell(["a1", "b2", "+"]), 33)
        self.assertEqual(s.eval_cell(["a2", "c3", "-"]), -21)
        self.assertEqual(s.eval_cell(["a3", "3", "*"]), 39)
        self.assertEqual(s.eval_cell(["44", "a1", "/"]), 4)
        self.assertEqual(s.eval_cell(["-1", "-2", "/"]), .5)
        self.assertEqual(s.eval_cell(["-1", "0", "/"]), "#ERR")
        self.assertEqual(s.eval_cell(["-1", "0"]), "#ERR")
        self.assertEqual(s.eval_cell(["-1", "0", "+", "+"]), "#ERR")
        self.assertEqual(s.eval_cell(["-1", "#ERR", "+"]), "#ERR")
        self.assertEqual(s.eval_cell([".5"]), "#ERR")
        self.assertEqual(s.eval_cell(["-.5"]), "#ERR")
        self.assertEqual(s.eval_cell(["5."]), "#ERR")
        self.assertEqual(s.eval_cell(["-5."]), "#ERR")
        self.assertEqual(s.eval_cell(["blah"]), "#ERR")
        self.assertEqual(s.eval_cell(["1", "blah"]), "#ERR")
        self.assertEqual(s.eval_cell(["1", "2", "%"]), "#ERR")
        self.assertEqual(s.eval_cell(["#ERR", "#ERR", "+"]), "#ERR")
        self.assertEqual(s.eval_cell(["005"]), 5)
        self.assertEqual(s.eval_cell(["5.00"]), 5)
        self.assertEqual(s.eval_cell(["-0"]), 0)
        self.assertEqual(s.eval_cell(["a1", "zzz999", "+"]), 11)
        self.assertEqual(s.eval_cell(["c4"]), 11)  # multiple indirect
        self.assertEqual(s.eval_cell(["c5"]), "#ERR")  # cycle!

    def test_compute(self):
        s = spreadsheet.Spreadsheet(
            ["b1 B2 +,2 b2 3 * -,3 ,+\n",
             "a1     ,5         ,  ,7 2 /\n",
             "C2 3 * ,1 2       ,  ,5 1 2 + 4 * + 3 -\n"
             ])
        self.assertEqual(s.value,
                         [
                             [-8, -13, 3, "#ERR"],
                             [-8, 5, 0, 3.5],
                             [0, "#ERR", 0, 14],
                         ])

    def test_to_String(self):
        s = spreadsheet.Spreadsheet(
            ["b1 B2 +,2 b2 3 * -,3 ,+\n",
             "a1     ,5         ,  ,7 2 /\n",
             "C2 3 * ,1 2       ,  ,5 1 2 + 4 * + 3 -\n"
             ])
        self.assertEqual(str(s),
                         '-8,-13,3,"#ERR"\n' +
                         '-8,5,0,3.5\n' +
                         '0,"#ERR",0,14\n'
                         )


def main():
    unittest.main()


if __name__ == "__main__":
    main()
