# coding=utf-8

import re

__author__ = 'charles haynes'

"""
Requirements
------------
1. Each cell is an expression in [[https://en.wikipedia.org/wiki/Reverse_Polish_notation][postfix notation]].
2. Each token in the expression will be separated by one or more spaces.
3. One cell can refer to another cell with the {LETTER}{NUMBER} notation (e.g. “A2”, “B4”– letters refer to columns, numbers to rows).
4. Expressions may include the basic arithmetic operators +, -, \*, /

Design
------
1. Read in and count each line
2. Split each line into cells separated by commas
3. Parse each cell
4. Evaluate the grid

Assumptions
-----------
1. Empty, missing, or undefined cells evaluate to 0
2. Table has as many rows as lines in the file
3. Rows may not all have the same number of columns
4. Badly formed expressions are #ERR
5. Divide by 0 is an #ERR
6. Any expression that depends on an #ERR is an #ERR
7. Cycles are possible, any cell in a cycle has a value of #ERR
8. Maximum number of rows is arbitrarily large, maximum number of columns is arbitrarily large
9. Numeric constants are of the form [-]?[0-9]+(.[0-9]+)?
   (Optional minus sign, some digits, optional decimal followed by some digits)
10. Empty file produces an empty result
11. cell names are not case sensitive ("A1" is the same as "a1")
"""

cell_name_pat = re.compile("^[a-z]+[1-9][0-9]*$")
const_pat = re.compile("^[-]?[0-9]+(?:\.[0-9]+)?$")


class Spreadsheet:
    def __init__(self, lines):
        """
        Create a new Spreadsheet instance

        :param lines: an iterator over lines of text containing spreadsheet cell definitions
        """
        self.d = {}
        self.val = None
        row = 0
        self.lines = []
        for line in lines:
            l = []
            row += 1
            cells = parse_line(line)
            col = 0
            for cell in cells:
                col += 1
                terms = parse_cell(cell)
                l.append(terms)
                if len(terms):
                    self.d[cell_name(row, col)] = terms
            self.lines.append(l)

    def __str__(self):  # kind of clever, and magic, and so bad. but only sort of
        """
        String converter for spreadsheets. Evaluate it and return as "csv"-ish representation.

        :return: String representation of this spreadsheet.
        """
        res = ""
        for row in self.value:
            r = ['"' + cell + '"' if type(cell) == str else str(cell) for cell in row]
            res += ",".join(r) + "\n"
        return res

    def _eval_ref(self, ref):
        """
        Evaluates a named cell reference, returning the value of that cell.

        :rtype : number or "#ERR"
        :param ref: the name of the referenced cell
        :return: the evaluated value of the cell
        """
        res = self.d.get(ref, 0)
        if type(res) != list:
            return res
        try:
            res = self.eval_cell(res)  # watch out for loops!
        except RuntimeError:
            res = "#ERR"  # this is a terrible way to catch cycles
        self.d[ref] = res  # memoize this result
        return res

    def eval_cell(self, cell):
        """
        Evaluate a cell's formula

        :rtype : number or "#ERR"
        :param cell: The formula for this cell
        :return: The value of that formula
        """
        stack = []
        for term in cell:
            if cell_name_pat.match(term):
                stack.append(self._eval_ref(term))
            elif const_pat.match(term):
                stack.append(_eval_const(term))
            else:
                _eval_op(stack, term)

        if len(stack) == 1:
            return stack[0]
        if len(stack) == 0:
            return 0
        return "#ERR"

    @property
    def value(self):
        """
        Evaluate all the cells in the spreadsheet and return it as a list of list of values. Values can be
        int, float, or the string "#ERR"

        :rtype : list of list of number or "#ERR"
        :return: the list of list of values
        """
        if self.val is not None:
            return self.val

        self.val = []
        for line in self.lines:
            row = []
            for cell in line:
                row.append(self.eval_cell(cell))
            self.val.append(row)
        return self.val


def _eval_op(stack, op):
    """
    Evaluate an operation. All operations are binary and postfix, so pop two operands off the stack, compute the result
    and push the result back. If there aren't enough operands, push an "#ERR". If either operand is "#ERR" then
    the result is "#ERR". Divide by zero is an "#ERR" The only legal operations are "+" numeric addition, "-" numeric
    subtraction, "*" numeric multiplication, and "/" numeric division. We go to some trouble to try to stay in
    the integer world whenever possible, only converting to float when actually required.

    :rtype : none, modifies the stack in place
    :param stack: the operation and operand stack.
    :param op:
    """
    if len(stack) < 2:  # everything is a binary operator and needs two operands
        stack.append("#ERR")
        return

    a = stack.pop()
    b = stack.pop()
    if a == "#ERR" or b == "#ERR":
        stack.append("#ERR")
    elif op == "+":
        stack.append(b + a)
    elif op == "-":
        stack.append(b - a)
    elif op == "*":
        stack.append(b * a)
    elif op == "/":
        try:
            if (b / a) * a == b:  # keep things int if we can
                stack.append(b / a)
            else:
                stack.append(float(b) / a)
        except ZeroDivisionError:
            stack.append("#ERR")
    else:  # unknown operator
        stack.append("#ERR")


def _eval_const(const):
    """
    Evaluate a numeric constant. They are either ints or floats, but prefer returning an int if we can.

    :rtype : number, int or float
    :param const: The string representation of the constant.
    :return: The numeric representation of the constant.
    """
    try:
        return int(const)  # keep things int if we can
    except ValueError:
        return float(const)


def parse_cell(cell):
    """
    Parse a textual representation of cell formula into its individual terms.

    :rtype : list of strings
    :param cell: a string containing a cell formula
    :return: a list of terms, lowerfied with whitespace stripped
    """
    return cell.split()


def parse_line(line):
    """
    Parse a textual representation of a list of cells into the individual cell formula strings.

    :rtype : list of strings
    :param line: a single line containing multiple cell formulas separated by commas optionally ending with "\n"
    :return: a list of cell formula strings
    """
    return [x.strip() for x in line.lower().split(",")]


def cell_name(row, col):
    """
    Given a row number (from 1 to arbitrarily large) and a column number (likewise) return the string cell name for
    that row and column. So row 1 column 1 is "a1" and row 28 column 13 would be "ab13"

    :rtype : basestring
    :param row: the integer row number (one based)
    :param col: the integer column number (one based)
    :return: a string representing the name of the cell at that row and column
    :raise ValueError:
    """
    if col <= 0 or row <= 0:
        raise ValueError
    letters = "abcdefghijklmnopqrstuvwxyz"
    res = ""
    while True:
        col -= 1
        res = letters[col % 26] + res
        col //= 26
        if col == 0:
            break
    return res + str(row)
