Running the app
===============
The main application is in `main.py` it reads test data from `test_data.sls`
Run it the standard way you'd run a python app:

    python main.py

The unit tests are in `test_spreadsheet.py` and can be run with:

    python test_spreadsheet.py

or

    python -m unittest discover
